<?php

/**
 * 系统首页
 */

namespace app\index\controller;

class IndexController extends \app\index\common\IndexCommon {


    /**
     * 首页
     */
    public function index() {
        parent::index();
    }

}